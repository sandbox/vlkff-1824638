<?php

/**
 * crm_core_profile_relation_save_record
 * Implements hook_form_FORM_ID_alter
 * Add relation settings to the profile settings page
 */
function crm_core_profile_relation_form_crm_core_profile_new_form_alter(&$form, &$form_state, $form_id) {
  // if the core profile form is not new
  $profile = array();
  if (array_key_exists('profile', $form_state)) {
    $profile = $form_state['profile'];
  }

  // get active profile contact type
  $profile_contact_type = NULL;
  if (!empty($form_state['values']['bundle_type'])) {
    $profile_contact_type = $form_state['values']['bundle_type'];
  }

  // load profile_relation if exists
  if (!empty($profile)) {
    $crm_core_profile_relation = crm_core_profile_relation_load_record($profile['name']);
    $form_state['crm_core_profile_relation'] = $crm_core_profile_relation;     
    $settings = $crm_core_profile_relation['settings'];
  }  

  // get default value for profile contact endpoint type (source/target)
  $endpoint_second_contact_source = 'autocomplete';
  if (!empty($settings)) {
    if ($settings['endpoints'][0] == 'profile_contact') {
      $endpoint_profile_contact_type = 'source';
      $endpoint_second_contact_source = $settings['endpoints'][1];
    }
    else {
      $endpoint_profile_contact_type = 'target';
      $endpoint_second_contact_source = $settings['endpoints'][0];
    }
  }

  // build form
  $form['relation'] = array(
    '#tree' => TRUE,
    '#title' => t('Relationship Information'),
    '#type' => 'fieldset',
    '#weight' => 6,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="crm-core-profile-relation">',
    '#suffix' => '</div>',
  );

  $relation_bundle_options = array('none' => t('None'));
  $relation_types = crm_core_relationship_load_relationship_types($profile_contact_type, 0);
  foreach ($relation_types as $relation_type) {
    $relation_bundle_options['source:'.$relation_type->relation_type] = $relation_type->label;
  }
  $relation_types = crm_core_relationship_load_relationship_types($profile_contact_type, 1);
  foreach ($relation_types as $relation_type) {
    $relation_bundle_options['target:'.$relation_type->relation_type] = $relation_type->reverse_label;
  }

  $bundle_type = empty($settings['bundle_type']) ? 'none' : $endpoint_profile_contact_type.':'.$settings['bundle_type'];
  // get active value for relation bundle (source/target)
  if (isset($form_state['input']['relation']['bundle_type'])) {
    $bundle_type = $form_state['input']['relation']['bundle_type'];
  }

  $form['relation']['bundle_type'] = array(
    '#type' => 'select',
    '#title' => t('Select the relationship type below'),
    '#description' => t('Select "none" for no action.'),
    '#options' => $relation_bundle_options,
    '#default_value' => $bundle_type,
    '#ajax' => array(
      'callback' => 'crm_core_profile_relation_ajax_callback',
      'wrapper' => 'crm-core-profile-relation',
      'method' => 'replace',
    ),
    '#prefix' => '<div id="crm-core-profile-relation-bundle-type">',
    '#suffix' => '</div>',
  );

  $form['relation']['endpoint_second_contact_source'] = array(
    '#type' => 'radios',
    '#title' => t('Select a source for related contact'),
    '#options' => array(
      'autocomplete' => t('Autocomplete field'),
      'url_variable' => t('Load from URL parameter (related_contact_id)'),
    ),
    '#default_value' => $endpoint_second_contact_source,
  );

  $form['relation']['fields'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $form['relation']['fields']['toggle'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );

  if ($bundle_type != 'none') {
    $bundle_parts = explode(':', $bundle_type);
    $bundle_relation_role = $bundle_parts[0];
    $bundle_name = $bundle_parts[1];
  } else {
    $bundle_relation_role = '';
    $bundle_name = '';
  }

  if (!empty($bundle_name) && $bundle_name != 'none') {
    $field_options = array();
    foreach(field_info_instances('relation', $bundle_name) as $field_name => $field) {
      $field_options[$field_name] = $field['label'];
    }
    unset($field_options['endpoints']);

    // specify default values for fields checkboxes
    $fields = array();
    if(!empty($settings)) {
      $fields = $settings['fields']['toggle'];
      foreach ($fields as $fieldname => $active) {
        if (!isset($field_options[$fieldname])) {
          unset($fields[$fieldname]);
        }
      }
    }

    $form['relation']['fields']['toggle'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Field selection'),
      '#options' => $field_options,
      '#description' => t('Check the relation fields that will appear on the profile form'),
      '#default_value' => $fields,
    );
  }

  // adding validation and submission handlers
  $form['#validate'][] = 'crm_core_profile_relation_settings_form_validate';
  $form['#submit'][] = 'crm_core_profile_relation_settings_form_submit';
}

/**
 * Implementation of hook_crm_core_profile_new_form_bundle_type_ajax_callback
 */
function crm_core_profile_relation_crm_core_profile_new_form_bundle_type_ajax_callback($form, $form_state) {
  $commands[] = ajax_command_replace(
    '#crm-core-profile-relation-bundle-type',
    drupal_render($form['relation']['bundle_type'])
  );
  $commands[] = ajax_command_replace(
    '#crm-core-profile-relation-endpoint-profile-contact-type',
    drupal_render($form['relation']['endpoint_profile_contact_type'])
  );
  return $commands;
}

/**
 * AJAX callback for $form[relation][bundle_type] form element
 */
function crm_core_profile_relation_ajax_callback($form, $form_state) {
  return $form['relation'];
}

 /**
  * Validation handler for profile settings form.
  */
 function crm_core_profile_relation_settings_form_validate($form, &$form_state) {
   // validate relation type and endpoints
 }

 /**
  * Submit handler for profile settings form.
  */
 function crm_core_profile_relation_settings_form_submit($form, &$form_state) {
   $profile_name = $form_state['values']['name'];

   if ($form_state['values']['relation']['bundle_type'] != 'none') {
     $bundle_parts = explode(':', $form_state['values']['relation']['bundle_type']);
     $bundle_relation_role = $bundle_parts[0];
     $bundle_name = $bundle_parts[1];

     $relation_profile['profile_name'] = $profile_name;
     $relation_profile['settings']['bundle_type'] = $bundle_name;
     $relation_profile['settings']['fields']['toggle'] = $form_state['values']['relation']['fields']['toggle'];

     if ($bundle_relation_role == 'source') {
       $relation_profile['settings']['endpoints'][0] = 'profile_contact';
       $relation_profile['settings']['endpoints'][1] = $form_state['values']['relation']['endpoint_second_contact_source'];
     }
     elseif ($bundle_relation_role == 'target') {
       $relation_profile['settings']['endpoints'][1] = 'profile_contact';
       $relation_profile['settings']['endpoints'][0] = $form_state['values']['relation']['endpoint_second_contact_source'];
     }

     // If crm_core_profile_relation module adds to profile entry form the relation autocomplete field,
     //   we should add it to fields list to sort if with other fields.
     foreach ($relation_profile['settings']['endpoints'] as $endpoint_index => $endpoint) {
       if ($endpoint == 'autocomplete') {
         $reversed_relation = ($relation_profile['settings']['endpoints'][0] == 'profile_contact') ? 0 : 1;
         $relation_profile['settings']['fields']['toggle'][$reversed_relation ? 'source' : 'target'] =
           $reversed_relation ? 'source' : 'target';
       }
     }

     crm_core_profile_relation_save_record($relation_profile);
   }
   else {
     crm_core_profile_relation_delete_record($profile_name);
   }
 }

/**
 * Implements hook_form_FORM_ID_alter for crm_core_profile_weight_form
 */
function crm_core_profile_relation_form_crm_core_profile_weight_form_alter(&$form, &$form_state) {
  // if the core profile form is not new
  $profile = array();
  if (array_key_exists('profile', $form_state)) {
    $profile = $form_state['profile'];
  }
  if (!empty($profile)) {
    if (array_key_exists('crm_core_profile_relation', $form_state)) {
      $crm_core_profile_relation =  $form_state['crm_core_profile_relation'];
    } else {
      $crm_core_profile_relation = crm_core_profile_relation_load_record($profile['name']);
      $form_state['crm_core_profile_relation'] = $crm_core_profile_relation;
    }
  }

  if(empty($crm_core_profile_relation)) {
    return;
  }

  $fields = $crm_core_profile_relation['settings']['fields'];
  $fields['weight'] = (isset($fields['weight'])) ? $fields['weight'] : array();

  $fields_toggle = array();
  // sort the toggles if weight has been assigned
  foreach($fields['weight'] as $field_name => $value) {
    $fields_toggle[$field_name] = $fields['toggle'][$field_name];
  }
  $fields_toggle = (!empty($fields_toggle)) ? $fields_toggle : $fields['toggle'];

  foreach ($fields_toggle as $field_name => $visibility) {
    if ($visibility !== 0) {
      $field = field_info_instance('relation', $field_name, $crm_core_profile_relation['settings']['bundle_type']);
      if ($field_name == 'source' || $field_name == 'target') {
        // ToDo: move that code to the function
        $reversed_relation = ($crm_core_profile_relation['settings']['endpoints'][0] == 'profile_contact') ? 0 : 1;
        $relation_type_name = $crm_core_profile_relation['settings']['bundle_type'];
        $relation_types = relation_get_types(array($relation_type_name));
        $relation_type = array_pop($relation_types);

        $field = array();
        $field['label'] = ($reversed_relation) ? $relation_type->reverse_label : $relation_type->label;
        $field['label'] .= ' '.t('(relation endpoint autocomplete)');
      }

      if (empty($field)) {
        continue;
      }

      $form['weight'][$field_name]['label'] = array('#markup' => check_plain($field['label']));
      $form['weight'][$field_name]['weight'] = array(
        '#type' => 'weight',
        '#title' => t('Weight for @title', array('@title' => $field['label'])),
        '#title_display' => 'invisible',
        '#default_value' => (array_key_exists($field_name, $fields['weight'])) ?  $fields['weight'][$field_name]['weight'] : -10,
      );
    }
  }

  $sort_keys = array();
  foreach($form['weight'] as $field_names => $values) {
    $sort_keys[$field_names] = $values['weight']['#default_value']['weight'];
  }

  asort($sort_keys);
  $sort_keys = array_keys($sort_keys);
  $ordered = array();
  foreach ($sort_keys as $field_name) {
    if (array_key_exists($field_name, $form['weight'])) {
      $ordered[$field_name] = $form['weight'][$field_name];
    }
  }

  $form['weight'] = $ordered;

  $form['#validate'][] = 'crm_core_profile_relation_weight_form_validate';
  $form['#submit'][] = 'crm_core_profile_relation_weight_form_submit';
}

/**
 * weight_form validation handler
 */
function crm_core_profile_relation_weight_form_validate($form, &$form_state) {

}

/**
 * weight_form submission handler
 */
function crm_core_profile_relation_weight_form_submit($form, &$form_state) {
  $crm_core_profile_relation = $form_state['crm_core_profile_relation'];
  $relation_fields = $crm_core_profile_relation['settings']['fields'];

  // save the weight order (just for relation fields)
  foreach ($form_state['values']['weight'] as $field => $weight) {
    if (array_key_exists($field, $relation_fields['toggle'])) {
      if($relation_fields['toggle'][$field] !== 0) {
        $sort_keys[$field] = $weight;
      }
    }
  }

  asort($sort_keys);

  // unset($form_state['values']['weight']);
  foreach($sort_keys as $field_name => $weight) {
    $weights[$field_name]['weight'] = $weight;
  }

  $relation_fields['weight'] = $weights;
  $crm_core_profile_relation['settings']['fields'] = $relation_fields;

  crm_core_profile_relation_save_record($crm_core_profile_relation);
}
